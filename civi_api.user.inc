<?php
/**
 */

/**
 * Given a UF user object, make sure there is a contact
 * object for this user. If the user has new values, we need
 * to update the CRM DB with the new values
 *
 * @global type $user
 * @param type $user
 */
function civi_ufmatch_sync($user = NULL) {
  if (civicrm_initialize()) {
    if (!$user) {
      global $user;
    }
    $contact_id = civi_get_contact_id($user->uid);

    civi_load_bao('UFMatch', 'Core');
    CRM_Core_BAO_UFMatch::synchronize($user, false, 'Drupal', civicrm_get_ctype( 'Individual'));
  }
}

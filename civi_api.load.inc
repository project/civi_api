<?php

/**
 * Load a CiviCRM object php file. Warning! All text is case sensitive!
 *
 * Example:
 * @code
 *   // Load CRM/Activity/BAO/Query.php.
 *   civi_load_include('Query', 'Activity', 'BAO')
 * @endcode
 *
 * @param $name
 *   The name of file without extension. CiviCRM files always have .php extension.
 * @param type $module
 *   The main module of CiviCRM, eg Group, Core, etc.
 * @param type $sub
 *   The subdirectory of module, where file is placed.
 * @param type $crmbase
 *  The base directory of CiviCRM
 *   @todo: i think, this is temporary. I've never seen that case, when we need
 *   include files outside this directory during drupal usage.
 * @return The name of the included file, if successful; FALSE otherwise.
 */
function civi_load_include($name, $module, $sub = NULL, $crmbase = 'CRM') {
  if (!is_null($sub)) {
    $sub = "/$sub";
  }
  $file = DRUPAL_ROOT . '/' . CIVICRM_DIR . '/' . $crmbase . '/' . $module . $sub . "/$name.php";
  if (is_file($file)) {
    require_once $file;
    return $file;
  }
  return FALSE;
}

/**
 * Load a BAO file of a CiviCRM object.
 *
 * @param $name
 *   The name of file without extension. CiviCRM files always have .php extension
 * @param $module
 *   The main module of CiviCRM, eg Group, Core, etc.
 * @return
 *   TRUE, if file is loaded succesful
 * @ingroup load_include
 */
function civi_load_bao($name, $module) {
  if (!civi_load_include($name, $module, 'BAO')) {
    drupal_set_message(t('The @module has not BAO extension with @name', array('@module' => $module, '@name' => $name)), 'error');
  }
  else {
    return TRUE;
  }
}

/**
 * Load a DAO file of a CiviCRM object.
 *
 * @param $name
 *   The name of file without extension. CiviCRM files always have .php extension
 * @param $module
 *   The main module of CiviCRM, eg Group, Core, etc.
 * @return
 *   TRUE, if file is loaded succesful
 * @ingroup load_include
 */
function civi_load_dao($name, $module) {
  if (!civi_load_include($name, $module, 'DAO')) {
    drupal_set_message(t('The @module has not DAO extension with @name', array('@module' => $module, '@name' => $name)), 'error');
  }
  else {
    return TRUE;
  }
}

/**
 * Load a Component file of a CiviCRM object.
 *
 * @param $name
 *   The name of file without extension. CiviCRM files always have .php extension
 * @param $module
 *   The main module of CiviCRM, eg Group, Core, etc.
 * @return
 *   TRUE, if file is loaded succesful
 * @ingroup load_include
 */
function civi_load_component($name, $module) {
  if (!civi_load_include($name, $module, 'Component')) {
    drupal_set_message(t('The @module has not compontent extension with @name', array('@module' => $module, '@name' => $name)), 'error');
  }
  else {
    return TRUE;
  }
}

/**
 * Load a Config file of a CiviCRM object.
 *
 * @param $name
 *   The name of file without extension. CiviCRM files always have .php extension
 * @param $module
 *   The main module of CiviCRM, eg Group, Core, etc.
 * @return
 *   TRUE, if file is loaded succesful
 * @ingroup load_include
 */
function civi_load_config($name, $module) {
  if (!civi_load_include($name, $module, 'Config')) {
    drupal_set_message(t('The @module has not config extension with @name', array('@module' => $module, '@name' => $name)), 'error');
  }
  else {
    return TRUE;
  }
}


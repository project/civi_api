<?php

/**
 * @file
 *   civi_api.db.inc
 * DB layer functions for CiviCRM
 *
 * @developers:
 *    Gabor Szanto <hello@szantogabor.com>
 *    http://szantogabor.com
 *
 */

/**
 * Provide database connection to CiviCRM database. If argument $function is
 * exists, the function is called, set the default Drupal database active and
 * and return the value of called funtion's result.
 *
 * *** TMP DEPRICATED ***
 * @todo figure out on the fly databaseconnection
 * WARNING, if no $function set, always must call db_set_active(); after this to
 * set default Drupal database active.
 *
 * Basic usage without $function:
 * @code
 *   civi_db_connect();
 *   $query = 'foo';
 *   $query->execute;
 *   db_set_active(); //Always call it, when no more database function need in
 * civicrm
 * @endcode
 *
 * Usage with $function:
 * @code
 *   function master() {
 *     $function = function() {
 *       $query = 'foo';
 *       $query->execute;
 *       return $query;
 *     }
 *     $master_result = civi_db_connect($function); // In this case the db_set_active()
 * need not.
 *     return $master_result;
 *   }
 *
 * civicrm
 * @param $function
 *   a string
 */
function civi_db_connect($function = NULL) {
  global $databases;

  $config = civi_get_config();
  $uf_dsn = $config->dsn;

  //@todo: need to fetch port number too, if exists??
  preg_match_all('/^(?P<driver>.*):\/\/(?P<username>.*):(?P<password>.*)@(?P<host>.*)\/(?P<database>.*)\?/', $uf_dsn, $db_data);

  $cividb = array(
    'default' => array(
      'port' => '',
      'prefix' => '',
    )
  );

  foreach ($db_data as $key => $value) {
    if (!is_numeric($key)) {
      $cividb['default'][$key] = $db_data[$key][0];
    }
  }
  variable_set('cividb', $cividb);

  db_set_active('cividb');

  if (is_callable($function)) {
    $return = $function;
    db_set_active();
    return $return;
  }
}
